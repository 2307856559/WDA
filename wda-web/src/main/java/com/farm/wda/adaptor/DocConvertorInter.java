package com.farm.wda.adaptor;

import java.io.File;

public interface DocConvertorInter {

	void run(File file, String fileTypeName, File targetFile);

}
